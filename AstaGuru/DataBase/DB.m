//
//  DB.m
//  AstaGuru
//
//  Created by Tejas Todkar on 16/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import "DB.h"
#define TableName @"Notification"

@interface DB()
@property dataBaseModel *dbModel;
@property (strong, nonatomic) NSMutableArray *dataBaseArray;
@end

@implementation DB


static DB *sharedManager = nil;

+ (DB *)sharedInstance
{
    if (sharedManager) {
        return sharedManager;
    }
    
    static dispatch_once_t  t = 0;
    dispatch_once(&t, ^{
        sharedManager = [[DB alloc] init];
    });
    
    return sharedManager;
}

-(NSString  *)filepathDB{
    NSArray *dir=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dbPath=[NSString stringWithFormat:@"%@/notification.sqlite",[dir lastObject]];
    return dbPath;
}

//MARK:- Sqlite table Creation

-(void)createTable{
    
    NSFileManager *file=[NSFileManager new];
    NSString *dbPath = [self filepathDB];
    if([file fileExistsAtPath:dbPath isDirectory:nil]){
        NSLog(@"Table is Allready Created");
        return;
    }
    if ([self openDB]){
        const char *query="create table Notification (Id INTEGER PRIMARY KEY AUTOINCREMENT,DeviceId VARCHAR(20),Title VARCHAR(20),Message VARCHAR(20),Date VARCHAR(20))";
        
        if (sqlite3_exec(_database, query, NULL, NULL, NULL)==SQLITE_OK){
            NSLog(@"User table created successfully..");
        }else{
            NSLog(@"User table creation failed..");
        }
    }else{
        NSLog(@"Open table failed..");
    }
    sqlite3_close(_database);
    
}
-(BOOL)openDB{
    if(sqlite3_open([[self filepathDB]UTF8String], &_database)==SQLITE_OK){
        NSLog(@"dataBaseOpen");
        return true;
    }else{
        sqlite3_close(_database);
        return false;
        NSAssert(0, @"Database failed to Open");
    }
}

-(void)addDatainDatabase:(NSString *)deviceID title:(NSString *)Title message:(NSString *)Message{
    int count = [self getNotificationCount];
    if(count > 40){
        [self deleteDataFromDB:deviceID dbTitle:Title dbMessage:Message];
    }else{
        [self insertdataIntoDB:deviceID dbTitle:Title dbMessage:Message];
    }
}

-(int)getNotificationCount{
  int count = 0;
  if ([self openDB]) {
    const char* sqlStatement = "SELECT COUNT(*) FROM Notification";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, sqlStatement, -1, &statement, NULL) ==
        SQLITE_OK) {
      while(sqlite3_step(statement) == SQLITE_ROW) {
        count = sqlite3_column_int(statement, 0);
      }
    }else{
        NSLog(@"Failed from sqlite3_prepare_v2. Error is:  %s",
              sqlite3_errmsg(_database));
    }
    sqlite3_finalize(statement);
    sqlite3_close(_database);
  }
  return count;
}

//MARK:- Insert and Delete Operation of Database

-(void)insertdataIntoDB:(NSString *)deviceId dbTitle:(NSString *)Title dbMessage:(NSString *)Message{
    char *error;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    if ([self openDB]) {
        NSString *inserStmt = [NSString stringWithFormat:@"INSERT INTO Notification (DeviceId,Title,Message,Date) VALUES (\"%@\", \"%@\", \"%@\", \"%@\")", deviceId, Title, Message,dateString];
        const char *insert_stmt = [inserStmt UTF8String];

        if (sqlite3_exec(_database, insert_stmt, NULL, NULL, &error)==SQLITE_OK) {
            NSLog(@"Insert successfully");
        }
        sqlite3_close(_database);
    }
}

-(void)deleteDataFromDB:(NSString *)deviceId dbTitle:(NSString *)Title dbMessage:(NSString *)Message{
    char *error;
    if ([self openDB]) {
        NSString *deleteStmt = [NSString
        stringWithFormat: @"delete from Notification order by id limit 1"];
        const char *delete_stmt = [deleteStmt UTF8String];
        if (sqlite3_exec(_database, delete_stmt, NULL, NULL, &error)==SQLITE_OK) {
            NSLog(@"delete successfully");
            [self insertdataIntoDB:deviceId dbTitle:Title dbMessage:Message];
        }
        sqlite3_close(_database);
    }
}


//MARK:- get Values From DataBase


-(NSMutableArray *)getDataFromDatabase{

    NSFileManager *fileManager = [NSFileManager defaultManager];
    _dataBaseArray = [[NSMutableArray alloc]init];

    if([fileManager fileExistsAtPath:[self filepathDB]]){
        if ([self openDB]){
            
            const char *insert_stmt = "SELECT * FROM Notification";
            sqlite3_stmt *statement;

            if (sqlite3_prepare_v2(_database, insert_stmt, -1, &statement, NULL) == SQLITE_OK) {

                while (sqlite3_step(statement) == SQLITE_ROW) {
                    _dbModel = [[dataBaseModel alloc]init];
                    _dbModel.deviceId = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 1)];
                    _dbModel.bidTitle = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 2)];
                    _dbModel.bidMessage = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 3)];
                    _dbModel.date = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 4)];
                    _dbModel.bidDetailMessage = @"";     /////***** In Future if Required then this will be added in DB****** Right Now its Not Addded In DB////////////
                    [_dataBaseArray addObject:_dbModel];
                    _dbModel = nil;

                }
             
            } else {
                NSLog(@"Unable to prepare statement: %s db err '%s' (%1d)", __FUNCTION__, sqlite3_errmsg(_database), sqlite3_errcode(_database));
            }
            sqlite3_finalize(statement);
            sqlite3_close(_database);
        } else {
            NSLog(@"Unable top open database.");
        }
    }else{
        
        NSLog(@"database is not exist.");
    }
    
    return _dataBaseArray;
  }


@end
