//
//  DB.h
//  AstaGuru
//
//  Created by Tejas Todkar on 16/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "dataBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DB : NSObject{
    sqlite3 *_database;
}


+ (DB *)sharedInstance;


-(void)createTable;
-(void)addDatainDatabase:(NSString *)deviceID title:(NSString *)Title message:(NSString *)Message;
-(int)getNotificationCount;
-(NSMutableArray *)getDataFromDatabase;

@end

NS_ASSUME_NONNULL_END
