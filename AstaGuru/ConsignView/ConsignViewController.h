//
//  ConsignViewController.h
//  AstaGuru
//
//  Created by Tejas Todkar on 02/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKDropdownMenu/MKDropdownMenu.h>


NS_ASSUME_NONNULL_BEGIN

@interface ConsignViewController : UIViewController<UIImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *categoryArray,*yearArray,*UnitArray,*imagedataArary,*imageArray,*currencyArray;
    int pageIndex;
    BOOL validation;
    NSString *userId;
}

//MARK:- CommonPoint

@property (weak, nonatomic) IBOutlet UIImageView *oneNoImageview;
@property (weak, nonatomic) IBOutlet UIImageView *twoNoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *threeNoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleheaderLbl;
- (IBAction)nextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextOutlet;




@property (weak, nonatomic) IBOutlet UIView *consignCategoryView;
@property (weak, nonatomic) IBOutlet UIView *consigndetailView;
@property (weak, nonatomic) IBOutlet UIView *consignPhotosView;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *categoryDropdown;
@property (weak, nonatomic) IBOutlet UILabel *categoryLbl;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;




//MARK:- Consign Detail View
@property (weak, nonatomic) IBOutlet UITextField *titleTxt;
@property (weak, nonatomic) IBOutlet UIButton *yearbtnOutlet;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *yearDropdown;
@property (weak, nonatomic) IBOutlet UITextField *mediumTxt;
@property (weak, nonatomic) IBOutlet UITextField *heightTxtOutlet;
@property (weak, nonatomic) IBOutlet UITextField *widthTxtoutlet;
@property (weak, nonatomic) IBOutlet UITextField *depthTxtOutlet;
@property (weak, nonatomic) IBOutlet UITextField *unitsTxtOutlet;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *unitsdropedown;

- (IBAction)thisIsanEdition:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *thisEditionOutlet;
@property (weak, nonatomic) IBOutlet UIView *editionStackview;
@property (weak, nonatomic) IBOutlet UIView *currencyView;



@property (weak, nonatomic) IBOutlet UITextField *editionNo;
@property (weak, nonatomic) IBOutlet UITextField *editionSize;
- (IBAction)workSignedYes:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *worksignYesOutlet;
@property (weak, nonatomic) IBOutlet UIButton *worksignNoOutlet;
- (IBAction)doesThisWorkAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *doesthisWorkYesOutlet;
@property (weak, nonatomic) IBOutlet UIButton *doesthisworkNoOutlet;
@property (weak, nonatomic) IBOutlet UITextField *acquireWorkTxtOutlet;
@property (weak, nonatomic) IBOutlet UITextField *workLocatedTxt;
- (IBAction)priceInMindAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *priceinMindYesOutlet;
@property (weak, nonatomic) IBOutlet UIButton *pricinMindNoOutlet;
@property (weak, nonatomic) IBOutlet UITextField *mobileTxt;
@property (weak, nonatomic) IBOutlet UITextView *additionalInfotxtView;
@property (weak, nonatomic) IBOutlet UIScrollView *contentDetailScrollView;
@property (weak, nonatomic) IBOutlet UILabel *currencylabel;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *currencyDropdown;
@property (weak, nonatomic) IBOutlet UITextField *priceTxt;




///MARK:- consignPhotos View
- (IBAction)uploadImageAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *uploadimgBtnOutlet;
@property (weak, nonatomic) IBOutlet UIView *stackcollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *photocollectionoutlet;


//MARK:- Required Label filed outlet

@property (weak, nonatomic) IBOutlet UILabel *titleRc;
@property (weak, nonatomic) IBOutlet UILabel *yearRC;
@property (weak, nonatomic) IBOutlet UILabel *mediumRc;
@property (weak, nonatomic) IBOutlet UILabel *heightRC;
@property (weak, nonatomic) IBOutlet UILabel *widthRc;
@property (weak, nonatomic) IBOutlet UILabel *editionRc;
@property (weak, nonatomic) IBOutlet UILabel *editionsizeRc;
@property (weak, nonatomic) IBOutlet UILabel *cityRc;
@property (weak, nonatomic) IBOutlet UILabel *currencyRc;
@property (weak, nonatomic) IBOutlet UILabel *mobilnoRc;
@property (weak, nonatomic) IBOutlet UILabel *priceRc;
@property (weak, nonatomic) IBOutlet UIView *alertView;
- (IBAction)okAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *blurView;


@end

NS_ASSUME_NONNULL_END
