//
//  consignInitialViewController.h
//  AstaGuru
//
//  Created by Tejas Todkar on 08/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface consignInitialViewController : UIViewController
- (IBAction)sellNowAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sellBtnOutlet;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

NS_ASSUME_NONNULL_END
