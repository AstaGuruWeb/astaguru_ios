//
//  ConsignViewController.m
//  AstaGuru
//
//  Created by Tejas Todkar on 02/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import "ConsignViewController.h"
#import "AFNetworking.h"
#import "ClsSetting.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "BforeLoginViewController.h"
#import "PhotocollCell.h"


@interface ConsignViewController ()

@end

@implementation ConsignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self setUpNavigationItem];
    [self initialSetup];
}


-(void)setUpNavigationItem
{
    self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-back-arrow-header"] style:UIBarButtonItemStyleDone target:self action:@selector(BackAction)];
    self.sidebarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    
    self.navigationItem.title = @"Sell";
    
}
-(void) viewWillAppear:(BOOL)animated{

}
-(void)initialSetup{
    categoryArray = [[NSMutableArray alloc]init];
    pageIndex = 0;
    
    _consignCategoryView.hidden = NO;
    _consigndetailView.hidden = YES;
    _consignPhotosView.hidden = YES;
    _stackcollectionView.hidden = YES;
    _alertView.hidden = YES;
    _blurView.hidden = YES;
    
    self.editionStackview.hidden = !self.thisEditionOutlet.selected;
    self.currencyView.hidden = !self.priceinMindYesOutlet.selected;
    
    _oneNoImageview.highlighted = YES;
    _twoNoImageView.highlighted = NO;
    _threeNoImageView.highlighted = NO;
    _titleheaderLbl.text = @"KINDLY SELECT YOUR DESIRED CATEGORY";
    
    yearArray = [[NSMutableArray alloc]initWithObjects:@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2011",@"2012",@"2013",@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021", nil
                 ];
    UnitArray = [[NSMutableArray alloc]initWithObjects:@"in",@"cm", nil];
    currencyArray = [[NSMutableArray alloc]initWithObjects:@"USD",@"RS", nil];
    imagedataArary = [[NSMutableArray alloc]init];
    imageArray = [[NSMutableArray alloc]init];
    
    _uploadimgBtnOutlet.layer.borderWidth = 1.0f;
    _uploadimgBtnOutlet.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    [_uploadimgBtnOutlet.layer setMasksToBounds:YES];
    
    _priceTxt.layer.borderWidth = 0.7f;
    _priceTxt.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    _priceTxt.layer.cornerRadius = 5.0f;
    [_priceTxt.layer setMasksToBounds:YES];
    
    _additionalInfotxtView.backgroundColor = [UIColor whiteColor];
    
    
    [self labelSetup];
    [self loadCategoryData];
}
-(void)labelSetup{
    self.categoryLbl.text = @"Select Category";
    self.currencylabel.text = @"Select Currency";
    self.unitsTxtOutlet.text = @"Select Unit";
    [self.yearbtnOutlet setTitle:@"Select Year" forState:UIControlStateNormal];
    
    
    _titleRc.hidden = YES;
    _yearRC.hidden =  YES;
    _mediumRc.hidden = YES;
    _heightRC.hidden = YES;
    _widthRc.hidden = YES;
    _editionRc.hidden = YES;
    _editionsizeRc.hidden = YES;
    _cityRc.hidden = YES;
    _currencyRc.hidden = YES;
    _priceRc.hidden = YES;
    _mobilnoRc.hidden = YES;
    
    validation = NO;//// Validation Bollean value for required field.
    
    
    
    
    
}

-(void)pageViewSetUp{
    [self.view endEditing:YES];
    if (pageIndex == 0) {
        //condition is category selected or not
        pageIndex = 1;
        _consignCategoryView.hidden = YES;
        _consigndetailView.hidden = NO;
        _consignPhotosView.hidden = YES;
        
        _oneNoImageview.highlighted = NO;
        _twoNoImageView.highlighted = YES;
        _threeNoImageView.highlighted = NO;
        _titleheaderLbl.text = @"ENTER DETAILS ABOUT THE WORK";
        
    } else if (pageIndex == 1) {
        pageIndex = 2;
        _consignCategoryView.hidden = YES;
        _consigndetailView.hidden = YES;
        _consignPhotosView.hidden = NO;
        
        _oneNoImageview.highlighted = NO;
        _twoNoImageView.highlighted = NO;
        _threeNoImageView.highlighted = YES;
        _titleheaderLbl.text = @"UPLOAD PHOTOS";
        
        [self.nextOutlet setTitle:@"Done" forState:UIControlStateNormal];
    }else{
        
    }
}

-(void)BackAction
{
    
    [self.view endEditing:YES];
    [self closeAllDropeDowm];
    if (pageIndex == 0) {
    [self.navigationController popViewControllerAnimated:YES];
        
    } else if (pageIndex == 2) {
        
        pageIndex = 1;
        _consignCategoryView.hidden = YES;
        _consigndetailView.hidden = NO;
        _consignPhotosView.hidden = YES;
        
        _oneNoImageview.highlighted = NO;
        _twoNoImageView.highlighted = YES;
        _threeNoImageView.highlighted = NO;
        _titleheaderLbl.text = @"ENTER DETAILS ABOUT THE WORK";
        [self.nextOutlet setTitle:@"NEXT" forState:UIControlStateNormal];
        
    }else{
        pageIndex = 0;
        _consignCategoryView.hidden = NO;
        _consigndetailView.hidden = YES;
        _consignPhotosView.hidden = YES;
        
        _oneNoImageview.highlighted = YES;
        _twoNoImageView.highlighted = NO;
        _threeNoImageView.highlighted = NO;
        _titleheaderLbl.text = @"KINDLY SELECT YOUR DESIRED CATEGORY";
        
    }
}

//MARK:- TextField & TextView  Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self closeAllDropeDowm];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self closeAllDropeDowm];
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField == _mobileTxt || textField == _heightTxtOutlet || textField == _widthTxtoutlet || textField == _depthTxtOutlet || textField == _priceTxt) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    if (textField == _mobileTxt){
        return newLength <= 10;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    [self closeAllDropeDowm];
    
    if (nextTag == 2 || nextTag == 6 || nextTag == 8 || nextTag == 10) {
        [textField resignFirstResponder];
        return YES;
    }
    
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder ) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}

-(BOOL)validateRequiredField
{
    _titleRc.hidden = YES;_yearRC.hidden =  YES;_mediumRc.hidden = YES;_heightRC.hidden = YES;_widthRc.hidden = YES;_editionRc.hidden = YES;_editionsizeRc.hidden = YES;_cityRc.hidden = YES;_currencyRc.hidden = YES;
    _priceRc.hidden = YES;_mobilnoRc.hidden = YES;
    validation = YES;
    
    
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_titleTxt.text].length==0){
        _titleRc.hidden = NO;
         validation = NO;
    }
     if ([_yearbtnOutlet.currentTitle isEqualToString:@"Select Year"] ){
        _yearRC.hidden = NO;
         validation = NO;
    }
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_mediumTxt.text].length==0){
        _mediumRc.hidden = NO;
         validation = NO;
    }if ([ClsSetting TrimWhiteSpaceAndNewLine:_heightTxtOutlet.text].length==0){
        _heightRC.hidden = NO;
         validation = NO;
    }if ([ClsSetting TrimWhiteSpaceAndNewLine:_widthTxtoutlet.text].length==0){
        _widthRc.hidden = NO;
         validation = NO;
    }if (_thisEditionOutlet.selected == YES){
        if ([ClsSetting TrimWhiteSpaceAndNewLine:_editionNo.text].length==0){
            _editionRc.hidden = NO;
             validation = NO;
        }
        if ([ClsSetting TrimWhiteSpaceAndNewLine:_editionSize.text].length==0){
            _editionsizeRc.hidden = NO;
             validation = NO;
        }
    }
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_workLocatedTxt.text].length==0){
        _cityRc.hidden = NO;
         validation = NO;
    }
    if (_priceinMindYesOutlet.selected == YES){
        if ([ClsSetting TrimWhiteSpaceAndNewLine:_priceTxt.text].length==0){
            _priceRc.hidden = NO;
             validation = NO;
        }
        if ([_currencylabel.text isEqualToString:@"Select Currency"]){
            _currencyRc.hidden = NO;
             validation = NO;
        }
    }
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_mobileTxt.text].length==0){
        _mobilnoRc.hidden = NO;
         validation = NO;
    }

    return  validation;
}


//MARK:-  DropDown method

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu{
    return  1;
    
}
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    if (dropdownMenu == _yearDropdown) {
        return yearArray.count ;
    }else if (dropdownMenu == _unitsdropedown){
        return UnitArray.count ;
    }else if (dropdownMenu == _currencyDropdown){
        return currencyArray.count;
    }else{
        return categoryArray.count;
    }
}


- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    
    if (dropdownMenu == _yearDropdown) {
        return [[NSAttributedString alloc] initWithString:@""
                                               attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight],
                                                            NSForegroundColorAttributeName: [UIColor blackColor]}];
    } else if (dropdownMenu == _unitsdropedown) {
        return [[NSAttributedString alloc] initWithString:@""
                                               attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight],
                                                            NSForegroundColorAttributeName: [UIColor blackColor]}];
    }else if (dropdownMenu == _currencyDropdown) {
        return [[NSAttributedString alloc] initWithString:@""
                                               attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight],
                                                            NSForegroundColorAttributeName: [UIColor blackColor]}];
    }else{
        return [[NSAttributedString alloc] initWithString:@""
                                               attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight],
                                                            NSForegroundColorAttributeName: [UIColor blackColor]}];
    }
    
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (dropdownMenu == _yearDropdown) {
        NSAttributedString *string =
         [[NSAttributedString alloc] initWithString:yearArray[row]
                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}];
        return string;
    } else if (dropdownMenu == _unitsdropedown) {
        NSAttributedString *string =
         [[NSAttributedString alloc] initWithString:UnitArray[row]
                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}];
        return string;
    }else if (dropdownMenu == _currencyDropdown) {
        NSAttributedString *string =
         [[NSAttributedString alloc] initWithString:currencyArray[row]
                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}];
        return string;
    }else{
        NSAttributedString *string =
         [[NSAttributedString alloc] initWithString:categoryArray[row]
                                         attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                      NSForegroundColorAttributeName: [UIColor blackColor]}];
        return string;
    }
   
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *selectedString;
    if (dropdownMenu == _yearDropdown) {
        
        selectedString = yearArray[row];
        [_yearbtnOutlet setTitle:selectedString forState:UIControlStateNormal];

    }else if (dropdownMenu == _unitsdropedown){
        selectedString = UnitArray[row];
        self.unitsTxtOutlet.text = selectedString;
       
    }
    else if (dropdownMenu == _currencyDropdown){
        selectedString = currencyArray[row];
        self.currencylabel.text = selectedString;
       
    }else{
        selectedString = categoryArray[row];
        self.categoryLbl.text = selectedString;
    }
        [dropdownMenu closeAllComponentsAnimated:YES];
}

-(void)closeAllDropeDowm{
    [_unitsdropedown closeAllComponentsAnimated:YES];
    [_currencyDropdown closeAllComponentsAnimated:YES];
    [_categoryDropdown closeAllComponentsAnimated:YES];
    [_yearDropdown closeAllComponentsAnimated:YES];
}

//MARK:- Button ACtions

- (IBAction)nextAction:(id)sender {
    [self closeAllDropeDowm];
    if ([self.nextOutlet.currentTitle  isEqual: @"Done"]) {
        pageIndex = 0;
        if (imagedataArary.count == 0) {
            [[[[iToast makeText:@"Please Select Image"] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
        }else{
            [self postConsignDataApi];
        }
    }else if (pageIndex == 1){
        if ([self validateRequiredField]) {
            [self pageViewSetUp];
        }else{
            [[[[iToast makeText:@"Please Filled all Required Fields "] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
        }
    } else {
        if ([self.categoryLbl.text isEqualToString:@"Select Category"]) {
            [[[[iToast makeText:@"Please Select Category"] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
            return;
        }
        userId = [[NSUserDefaults standardUserDefaults] valueForKey:USER_id] ;
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:USER_id] intValue] == 0) {
            [[[[iToast makeText:@"Please LogIn first"] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
            BforeLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BforeLoginViewController"];
            [self.navigationController pushViewController:rootViewController animated:YES];
            return;
        }
        [self pageViewSetUp];
    }
}
- (IBAction)thisIsanEdition:(id)sender {
    self.thisEditionOutlet.selected = !self.thisEditionOutlet.selected;
    self.editionStackview.hidden = !self.thisEditionOutlet.selected;
}
- (IBAction)workSignedYes:(id)sender {
    self.worksignNoOutlet.selected = !self.worksignNoOutlet.selected;
    self.worksignYesOutlet.selected = !self.worksignYesOutlet.selected;
    
}
- (IBAction)doesThisWorkAction:(id)sender {
    self.doesthisworkNoOutlet.selected = !self.doesthisworkNoOutlet.selected;
    self.doesthisWorkYesOutlet.selected = !self.doesthisWorkYesOutlet.selected;
    
}
- (IBAction)priceInMindAction:(id)sender {
    self.pricinMindNoOutlet.selected = !self.pricinMindNoOutlet.selected;
    self.priceinMindYesOutlet.selected = !self.priceinMindYesOutlet.selected;
    self.currencyView.hidden = !self.priceinMindYesOutlet.selected;
}
- (IBAction)uploadImageAction:(id)sender {
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
           pickerView.allowsEditing = YES;
           [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
           pickerView.delegate = self;
           [self presentViewController:pickerView animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

     UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
     NSString *encodedImg = [self encodeToBase64String:img];
    [imagedataArary addObject:encodedImg];
    [imageArray addObject:img];
    [self dismissViewControllerAnimated:YES completion:nil];
    _stackcollectionView.hidden = NO;
    [_photocollectionoutlet reloadData];
    
}

- (NSString *)encodeToBase64String:(UIImage *)image {
 return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}



//MARK:- API Calls .

-(void)loadCategoryData
{
    @try {
        NSMutableDictionary *Discparam = [[NSMutableDictionary alloc]init];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        NSString  *strQuery=[NSString stringWithFormat:@"%@",[ClsSetting getCategoryUrl]];
        NSString *url = strQuery;
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSError *error;
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             categoryArray = [[[dict valueForKey:@"resource"]valueForKey:@"category"]mutableCopy];
            [_categoryDropdown reloadAllComponents];
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [ClsSetting ValidationPromt:error.localizedDescription];
             }];
    }
    @catch (NSException *exception)
    {
        
    }
}

-(void)postConsignDataApi
{
    
    NSString *URLString = [NSString stringWithFormat:@"%@",[ClsSetting postConsignUrl]];
    NSDictionary *apiParam  = [self getConsignPostdata];
    ClsSetting *CIS=[[ClsSetting alloc] init];
    CIS.passResponseDataDelegate = self;
    [CIS callPostWeb:apiParam url:URLString view:self.view];
}

-(NSDictionary *)getConsignPostdata{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"Title"] = self.titleTxt.text;
    params[@"category"] = self.categoryLbl.text;
    params[@"Year"] = self.yearbtnOutlet.currentTitle;
    params[@"Medium"] = self.mediumTxt.text;
    params[@"Height"] = self.heightTxtOutlet.text;
    params[@"Width"] = self.widthTxtoutlet.text;
    params[@"Depth"] = self.depthTxtOutlet.text;
    params[@"Units"] = self.unitsTxtOutlet.text;
    params[@"EditionNumaber"] = self.thisEditionOutlet.isSelected == YES ? self.editionNo.text:@"NA";
    params[@"EditionSize"] = self.thisEditionOutlet.isSelected == YES ? self.editionSize.text:@"NA";;
    params[@"WorkSign"] = self.worksignYesOutlet.selected == YES  ? @"YES":@"NO";
    params[@"AquireWork"] = self.acquireWorkTxtOutlet.text;
    params[@"CityLocated"] = self.workLocatedTxt.text;
    params[@"Pricemind"] = self.priceinMindYesOutlet.selected == YES ?@"YES":@"NO" ;
    params[@"PriceCurrency"] = self.priceinMindYesOutlet.selected == YES ?self.currencylabel.text:@"NA";
    params[@"Price"] = self.priceinMindYesOutlet.selected == YES ?self.priceTxt.text:@"NA";;
    params[@"MobileNumber"] = self.mobileTxt.text;
    params[@"MoreInformation"] = self.additionalInfotxtView.text;
    params[@"Userid"] = userId ;
    params[@"MultiImage"] = imagedataArary;
    
    NSMutableArray *arr = [NSMutableArray arrayWithObjects:params,nil];
    NSDictionary *finalParameter = @{@"resource":arr};
    return  finalParameter;
}

-(void)passPostResponseData:(id)responseObject
{
    NSLog(@"%@",responseObject[@"Message"]);
    if ([responseObject[@"Message"] isEqualToString:@"Data Save Successfully"]) {
        _alertView.hidden = NO;
        _blurView.hidden = NO;
    }
}





//MARK:- Collection Methods (CollectionViewData Source)
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imageArray.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"photoCell";
    PhotocollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.collImgView.image = [imageArray objectAtIndex:indexPath.row];
    [cell.cancelButtonCollection addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
    cell.cancelButtonCollection.tag = indexPath.row;
    
    return cell;
}

-(void)deleteImage:(UIButton *)button{
    [imageArray removeObjectAtIndex:button.tag];
    [imagedataArary removeObjectAtIndex:button.tag];
    [_photocollectionoutlet reloadData];
    
    if (imageArray.count == 0) {
        _stackcollectionView.hidden = YES;
    }
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat Width = (self.view.frame.size.width-60)/3;
    return CGSizeMake(Width, Width);
}


- (IBAction)okAction:(UIButton *)sender {
    _alertView.hidden = YES;
    _blurView.hidden = YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController = rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
}
@end
