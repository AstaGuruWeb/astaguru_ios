//
//  NotifiactionManger.h
//  AstaGuru
//
//  Created by Tejas Todkar on 17/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "clsCurrentOccution.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotifiactionManger : NSObject

@property (strong, nonatomic) NSMutableArray *finalNotArray, *saveNotiArray;
+ (NotifiactionManger *)sharedInstance;

-(void)sendNotificationMsg:(NSString *)Title messageInfo:(NSString *)Message;
-(void)lotWonNotification:(NSMutableArray *)lotWonArray;


@end

NS_ASSUME_NONNULL_END
