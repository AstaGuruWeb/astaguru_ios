//
//  NotifiactionManger.m
//  AstaGuru
//
//  Created by Tejas Todkar on 17/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import "NotifiactionManger.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "DB.h"
#import "ClsSetting.h"

@implementation NotifiactionManger


static NotifiactionManger *sharedManager = nil;

+ (NotifiactionManger *)sharedInstance
{
    if (sharedManager) {
        return sharedManager;
    }
    static dispatch_once_t  t = 0;
    dispatch_once(&t, ^{
        sharedManager = [[NotifiactionManger alloc] init];
    });
    return sharedManager;
}



-(void)lotWonNotification:(NSMutableArray *)lotWonArray{
    
    NSInteger userId =  [[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue];
    _finalNotArray = [[NSMutableArray alloc]init];
    _saveNotiArray = [[NSMutableArray alloc]init];
    
    for (clsCurrentOccution *obj in lotWonArray) {
        if ([obj.strtimeRemains intValue] < 0 && userId == [obj.strmyuserid intValue]  && userId != 0 && [obj.strpricelow intValue] < [obj.strpricers intValue]) {
            _saveNotiArray = [[[NSUserDefaults standardUserDefaults]valueForKey:@"savedNotificationlist"]mutableCopy];
            if (_saveNotiArray.count > 0 && ![_saveNotiArray containsObject:obj.strproductid]){
                
                [_saveNotiArray addObject:obj.strproductid];
                [[NSUserDefaults standardUserDefaults]setObject:_saveNotiArray forKey:@"savedNotificationlist"];
                [_finalNotArray addObject:obj];
                
            }else{
                if ([_saveNotiArray containsObject:obj.strproductid]) {
                    
                }else{
                    if (_saveNotiArray.count == 0) {
                        _saveNotiArray = [[[NSMutableArray alloc]initWithObjects:obj.strproductid, nil]mutableCopy];
                        [[NSUserDefaults standardUserDefaults]setObject:_saveNotiArray forKey:@"savedNotificationlist"];
                    }else{
                    [_saveNotiArray addObject:obj.strproductid];
                    [[NSUserDefaults standardUserDefaults]setObject:_saveNotiArray forKey:@"savedNotificationlist"];
                    }
                }
                
            }
        }
    }
    
    if (_finalNotArray.count > 0) {
       __block NSInteger Count;
        Count = 0;
        
        for (int j = 0; j < _finalNotArray.count; j++) {
            clsCurrentOccution *obj = [[clsCurrentOccution alloc]init];
            obj = [_finalNotArray objectAtIndex:j];
            NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];
            NSString *strUserName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_NAME];
            NSString *NotificationMessage = [NSString stringWithFormat:@"Dear %@ %@, congratulations for winning Lot No.%@ (%@).Please view details of the lot in My purchase section. Thank you for bidding with AstaGuru.",strUserFirstName,strUserName,obj.strReference,obj.strtitle];
            
            [self sendNotificationMsg:@"Lot Won" messageInfo:NotificationMessage success:^(id responseObject) {
                if (responseObject) {
                    Count ++;
                }
            }];
            if (j < Count) {
                j++;
            }
            
        }
    }
    
}

-(void)sendNotificationMsg:(NSString *)Title messageInfo:(NSString *)Message success:(void (^)(id responseObject))success{
    @try {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"FcMToken"];
        if (deviceToken == nil){
            deviceToken = @"";
        }
        
        NSDictionary *dict = @{@"device_id": deviceToken,@"subject":Title,@"description": Message};
        
        
        NSString  *url=[NSString stringWithFormat:@"%@%@",[ClsSetting defaultURL],[ClsSetting sendNotificationURL]];
        
        [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            NSError *error;
            NSDictionary *dictResult = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
            success(dictResult);
            
            [[DB sharedInstance]addDatainDatabase:deviceToken title:Title message:Message];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}




-(void)sendNotificationMsg:(NSString *)Title messageInfo:(NSString *)Message{
    @try {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"FcMToken"];
        if (deviceToken == nil){
            deviceToken = @"";
        }
        
        NSDictionary *dict = @{@"device_id": deviceToken,@"subject":Title,@"description": Message};
        NSString  *url=[NSString stringWithFormat:@"%@%@",[ClsSetting defaultURL],[ClsSetting sendNotificationURL]];
        
        [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            NSError *error;
            NSDictionary *dictResult = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
            
            [[DB sharedInstance]addDatainDatabase:deviceToken title:Title message:Message];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

@end
