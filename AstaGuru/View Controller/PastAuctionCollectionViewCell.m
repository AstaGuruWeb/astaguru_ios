//
//  PastAuctionCollectionViewCell.m
//  AstaGuru
//
//  Created by Aarya Tech on 19/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "PastAuctionCollectionViewCell.h"

@implementation PastAuctionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        _lcImageH.constant = 160;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self layoutIfNeeded];
}

@end
