//
//  MyProfileViewController.h
//  AstaGuru
//
//  Created by sumit mashalkar on 15/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EGOImageView.h"
#import "JVFloatLabeledTextField.h"

@interface MyProfileViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

//Personal details
@property (weak, nonatomic) IBOutlet UIView *view_firstName;
@property (weak, nonatomic) IBOutlet UITextField *txt_firstName;

@property (weak, nonatomic) IBOutlet UIView *view_lastName;
@property (weak, nonatomic) IBOutlet UITextField *txt_lastName;

@property (weak, nonatomic) IBOutlet UIView *view_mobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNumber;

@property (weak, nonatomic) IBOutlet UIView *view_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;

@property (weak, nonatomic) IBOutlet UIView *view_bidLimit;
@property (weak, nonatomic) IBOutlet UITextField *txt_bidLimit;


//Profile




@property (weak, nonatomic) IBOutlet UIButton *btn_male;
- (IBAction)btn_male_pressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_female;
- (IBAction)btn_female_pressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_other;
- (IBAction)btn_other_pressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *view_birthDay;
@property (weak, nonatomic) IBOutlet UITextField *txt_birthDay;

@property (weak, nonatomic) IBOutlet UIView *view_birthMonth;
@property (weak, nonatomic) IBOutlet UITextField *txt_birthMonth;

@property (weak, nonatomic) IBOutlet UIView *view_birthYear;
@property (weak, nonatomic) IBOutlet UITextField *txt_birthYear;

@property (weak, nonatomic) IBOutlet UITextField *txt_aboutUs;
@property (weak, nonatomic) IBOutlet UIView *view_aboutUs;

@property (weak, nonatomic) IBOutlet UITextField *txt_interestedIn;
@property (weak, nonatomic) IBOutlet UIView *view_insterstedIn;

//Primary postal address
@property (weak, nonatomic) IBOutlet UIView *view_postalAddressLine1;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalAddressLine1;

@property (weak, nonatomic) IBOutlet UIView *view_postalAddressLine2;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalAddressLine2;

@property (weak, nonatomic) IBOutlet UIView *view_postalCountry;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalCountry;

@property (weak, nonatomic) IBOutlet UIView *view_postalState;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalState;

@property (weak, nonatomic) IBOutlet UIView *view_postalCity;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalCity;

@property (weak, nonatomic) IBOutlet UIView *view_postalZip;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalZip;

@property (weak, nonatomic) IBOutlet UIView *view_postalGstin;
@property (weak, nonatomic) IBOutlet UITextField *txt_postalGstin;

@property (weak, nonatomic) IBOutlet UIImageView *imgView_checkBillingAddress;
- (IBAction)btn_differentBillingAddres_pressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *view_billingAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_billingAddress_height;

@property (strong, nonatomic) IBOutlet UIView *view_billingName;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingName;

@property (weak, nonatomic) IBOutlet UIView *view_billingAddressLine1;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingAddressLine1;

@property (weak, nonatomic) IBOutlet UIView *view_billingAddressLine2;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingAddressLine2;

@property (weak, nonatomic) IBOutlet UIView *view_billingCountry;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingCountry;

@property (weak, nonatomic) IBOutlet UIView *view_billingState;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingState;

@property (weak, nonatomic) IBOutlet UIView *view_billingCity;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingCity;

@property (weak, nonatomic) IBOutlet UIView *view_billingZip;
@property (weak, nonatomic) IBOutlet UITextField *txt_billingZip;

@property (weak, nonatomic) IBOutlet UIView *view_userName;
@property (weak, nonatomic) IBOutlet UITextField *txt_userName;

@property (weak, nonatomic) IBOutlet UIView *view_password;
@property (weak, nonatomic) IBOutlet UITextField *txt_password;

@property (weak, nonatomic) IBOutlet UIView *view_nickName;
@property (weak, nonatomic) IBOutlet UITextField *txt_nickName;

@property (weak, nonatomic) IBOutlet UIView *view_panCard;
@property (weak, nonatomic) IBOutlet UITextField *txt_panCard;

@property (weak, nonatomic) IBOutlet UIView *view_panCardPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbl_panCardImageName;
@property (weak, nonatomic) IBOutlet EGOImageView *imgView_panCard;

@property (weak, nonatomic) IBOutlet UIView *view_aadharCard;
@property (weak, nonatomic) IBOutlet UITextField *txt_aadharCard;

@property (weak, nonatomic) IBOutlet UIView *view_aadharPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbl_aadharCardImageName;
@property (weak, nonatomic) IBOutlet EGOImageView *imgView_aadhar;

@property (weak, nonatomic) IBOutlet UIView *view_accountNo;
@property (weak, nonatomic) IBOutlet UITextField *txt_accountNo;

@property (weak, nonatomic) IBOutlet UIView *view_aacountHolderName;
@property (weak, nonatomic) IBOutlet UITextField *txt_accountHolderName;

@property (weak, nonatomic) IBOutlet UIView *view_ifscCode;
@property (weak, nonatomic) IBOutlet UITextField *txt_ifscCode;

@property (weak, nonatomic) IBOutlet UIView *view_branchName;
@property (weak, nonatomic) IBOutlet UITextField *txt_branchName;

@property (weak, nonatomic) IBOutlet UIView *view_swiftCode;
@property (weak, nonatomic) IBOutlet UITextField *txt_swiftCode;





@end
