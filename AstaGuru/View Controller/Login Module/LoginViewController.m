//
//  LoginViewController.m
//  AstaGuru
//
//  Created by Aarya Tech on 07/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "LoginViewController.h"
#import "SWRevealViewController.h"
#import "RegistrationViewController.h"
#import "ViewController.h"
#import "AfterLoginViewController.h"
#import "ForGotViewController.h"
#import "VerificationViewController.h"
#import "AppDelegate.h"
@interface LoginViewController ()<PassResponse>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationItem];
    // Do any additional setup after loading the view.
}
-(void)setUpNavigationItem
{
    self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"signs"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    self.sidebarButton.tintColor=[UIColor whiteColor];
    self.title=@"Sign In";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.revealViewController setFrontViewController:self.navigationController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
   
    
    //self.navigationItem.title=@"Sign Up";
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
    
    [ClsSetting SetBorder:_userName_View cornerRadius:2 borderWidth:1 color:[UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1]];

    [ClsSetting SetBorder:_password_View cornerRadius:2 borderWidth:1 color:[UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1]];

}
-(void)searchPressed
{
    
}
-(void)myastaguru
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)closePressed
{
    if (_IsCommingFromSideMenu==1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        
        
        [self.navigationController setViewControllers: @[rootViewController] animated: YES];
        
        [self.revealViewController setFrontViewController:self.navigationController];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSignInPressed:(id)sender
{
    if (_txtUserName.text.length == 0)
    {
        [ClsSetting ValidationPromt:@"Enter Username"];
    }
    else if (_txtPassword.text.length == 0)
    {
        [ClsSetting ValidationPromt:@"Enter Password"];
    }
    else
    {
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
        if (deviceToken == nil)
        {
            deviceToken = @"";
        }
        
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"loading";
        
       NSDictionary *Discparam = @{
                      @"username":[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text],
                      @"password":[ClsSetting TrimWhiteSpaceAndNewLine:_txtPassword.text],
                      @"devicetoken":deviceToken,
                      @"DeviceType":@"IOS"
        };
        NSString *url = [NSString stringWithFormat:@"%@",[ClsSetting loginURL]];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
        [manager POST:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@",responseObject);
            NSError *error;
            id dict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
            [self passGetResponseData:dict];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [ClsSetting ValidationPromt:error.localizedDescription];
        }];
    }
}

-(void)saveDevice: (NSString*)userID {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"FcMToken"];
    if (deviceToken == nil)
    {
        deviceToken = @"";
    }
    NSString *strQuery=[NSString stringWithFormat:@"%@", [ClsSetting saveDeviceURL]];
    NSString *url = strQuery;
    NSLog(@"%@",url);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    NSDictionary *dict = @{
                                  @"userid": userID,
                                  @"device_id": deviceToken,
                                  @"device_type": @"iOS",
                                  };
    NSLog(@"Dict %@",dict);
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
        
}



- (IBAction)ForgotPasswordpressed:(id)sender
{
    ForGotViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ForGotViewController"];
    [self.navigationController pushViewController:rootViewController animated:YES];
}

- (IBAction)SugnUpPressed:(id)sender
{
    RegistrationViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    [self.navigationController pushViewController:rootViewController animated:YES];
}

-(void)passGetResponseData:(id)responseObject
{
    NSMutableArray *arr1 = responseObject;
    if (arr1.count>0)
    {
        NSMutableDictionary *dict=[arr1 objectAtIndex:0];
        NSLog(@"%@",dict);
        
        if ([[dict valueForKey:@"password"] isEqualToString:[ClsSetting TrimWhiteSpaceAndNewLine:_txtPassword.text]])
        {
            dict=[ClsSetting RemoveNullOnly:dict];
            
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"username"] forKey:USER_NAME];
             [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"userid"] forKey:USER_id];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"nickname"] forKey:USER_nicName];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"name"] forKey:USER_FirstName];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"lastname"] forKey:USER_LastName];
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_nicName]);
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_NAME]);
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_FirstName]);
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USER_LastName]);
            
            
            NSString *str_userid = [dict valueForKey:@"userid"];
            [self saveDevice:str_userid];
            
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"confirmbid"] forKey:@"confirmbid"];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"EmailVerified"] forKey:@"EmailVerified"];
            [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"MobileVerified"] forKey:@"MobileVerified"];

            [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"user"];

            [[NSUserDefaults standardUserDefaults] setValue:[dict valueForKey:@"amountlimt"] forKey:@"amountlimt"];
            
            [[NSUserDefaults standardUserDefaults] setValue:[dict valueForKey:@"isOldUser"] forKey:@"isOldUser"];

            if ([dict[@"EmailVerified"]  isEqual: @"True"] && [dict[@"MobileVerified"]   isEqual: @"True"])
            {
                [ClsSetting ValidationPromt:@"Login Successfully"];
//                NSTimeInterval seconds = [NSDate timeIntervalSinceReferenceDate];
//                double milliseconds = seconds*1000;
                NSDate *date = [NSDate date];
              long milliseconds =  [date timeIntervalSince1970]*1000;
                NSLog(@"%ld",milliseconds);
                long loginTime = milliseconds+48*60*60*1000;
                NSLog(@"%ld",loginTime);
                
                [[NSUserDefaults standardUserDefaults]
                    setObject:[NSNumber numberWithLongLong:loginTime]
                    forKey:@"Logintime"
                ];
//               long HighScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Logintime"] longLongValue];
//              NSLog(@"%ld",HighScore);
                
                if (_IsCommingFromSideMenu==1)
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    
                    [self.navigationController setViewControllers: @[rootViewController] animated: YES];
                    
                    [self.revealViewController setFrontViewController:self.navigationController];
                    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                }
                else
                {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
            else
            {
                [ClsSetting ValidationPromt:@"Your are not Verified"];

                NSString *strSMSCode = [NSString stringWithFormat:@"%d",arc4random() % 9000 + 1000];
                NSString *strEmailCode = [NSString stringWithFormat:@"%d",arc4random() % 9000 + 1000];
                VerificationViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
                rootViewController.dict=dict;
                rootViewController.strEmail=[ClsSetting TrimWhiteSpaceAndNewLine:dict[@"email"]];
                rootViewController.strMobile=[ClsSetting TrimWhiteSpaceAndNewLine:dict[@"Mobile"]];
                rootViewController.strname=dict[@"t_firstname"];
                rootViewController.strSMSCode=strSMSCode;
                rootViewController.strEmialCode=strEmailCode;
                rootViewController.isRegistration = NO;
                rootViewController.IsCommingFromLoging = 1;
                [self.navigationController pushViewController:rootViewController animated:YES];

            }
        }
        else
        {
            [ClsSetting ValidationPromt:@"Invalid Username or Password"];
        }
    }
    else
    {
        [ClsSetting ValidationPromt:@"Invalid Username or Password"];
    }
}

@end
