//
//  SearchResultsTableViewController.m
//  Sample-UISearchController
//
//  Created by James Dempsey on 9/20/14.
//  Copyright (c) 2014 Tapas Software. All rights reserved.
//

#import "SearchResultsTableViewController.h"
#import "Country.h"
@implementation SearchResultsTableViewController
-(void)viewDidLoad
{
    [super viewDidLoad];

    //self.tableView.tableHeaderView = [[UIView alloc] init];
    [self.tableView setContentInset:UIEdgeInsetsMake(20, 0, 0, 0)];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SearchResultCell" forIndexPath:indexPath];

    //Country *country = [self.searchResults objectAtIndex:indexPath.row];

    //cell.textLabel.text = country.countryname;

    cell.textLabel.text = self.searchResults[indexPath.row];
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchResultsDelegate didSelectSearchResult:self.searchResults[indexPath.row]];
}


@end
