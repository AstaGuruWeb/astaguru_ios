//
//  CommonWebviewViewController.m
//  AstaGuru
//
//  Created by macbook on 09/04/20.
//  Copyright © 2020 4Fox Solutions. All rights reserved.
//

#import "CommonWebviewViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"

@interface CommonWebviewViewController ()

@end

@implementation CommonWebviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationItem];
    _ActivityIndicator.hidden=NO;
    _ActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view bringSubviewToFront:_ActivityIndicator];
    [_webview loadRequest:[NSURLRequest requestWithURL:_url]];
    [_ActivityIndicator startAnimating];
}

-(void)setUpNavigationItem
{
//    self.navigationItem.title= _navTitle;
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    
    self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"signs"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    self.sidebarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)closePressed
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController =rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_ActivityIndicator stopAnimating];
    _ActivityIndicator.hidden=YES;
}
 -(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_ActivityIndicator stopAnimating];
    _ActivityIndicator.hidden=YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webVie
{
//    [_ActivityIndicator stopAnimating];
//    _ActivityIndicator.hidden=YES;
}

@end
