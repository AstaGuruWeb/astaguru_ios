//
//  AuctionAnalysisTableViewCell.h
//  AstaGuru
//
//  Created by Apple.Inc on 16/12/17.
//  Copyright © 2017 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuctionAnalysisTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_astristName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_us;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rs;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lotno;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@end
