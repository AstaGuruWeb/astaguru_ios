//
//  CategoryListSidePageViewController.m
//  AstaGuru
//
//  Created by Aarya Tech on 05/10/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "CategoryListSidePageViewController.h"
#import "ClsSetting.h"
#import "SWRevealViewController.h"
#import "clsCategory.h"
#import "AppDelegate.h"
#import "CategoryDetailViewController.h"
#import "CommonWebviewViewController.h"
#import "WebViewViewController.h"

@interface CategoryListSidePageViewController ()<PassResponse>
{
    
}
@end

@implementation CategoryListSidePageViewController
@synthesize arrCategory;
- (void)viewDidLoad
{
    [super viewDidLoad];
//    arrCategory = [[NSMutableArray alloc]init];
    NSLog(@"%@",arrCategory);
//    [self getCategoryData];
    [self setUpNavigationItem];
    // Do any additional setup after loading the view.
}
-(void)getCategoryData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    objSetting.passResponseDataDelegate=self;
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"webcategory?api_key=%@",[ClsSetting apiKey]] view:self.view];
}

//-(void)passReseposeData1:(id)str
- (void)passPostResponseData:(id)responseObject
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpNavigationItem
{
    self.navigationItem.title=@"Categories";
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    
    self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"signs"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    self.sidebarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
-(void)closePressed
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController =rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
    
}
//-(void)passReseposeData:(id)arr
- (void)passGetResponseData:(id)responseObject
{
//    NSError *error;
//    NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
    
    //NSLog(@"%@",dict1);
    NSMutableArray *arrItemCount=[[NSMutableArray alloc]init];
    // arrItemCount=[parese parsePastOccution:[dict1 valueForKey:@"resource"]];
    arrItemCount=[parese parseCategory:[responseObject valueForKey:@"resource"]];
    
    [arrCategory addObjectsFromArray:arrItemCount];
    _clvCategoryList.hidden=NO;
    [_clvCategoryList reloadData];
}

#pragma mark- CollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (CGSize)collectionView:(UICollectionView *)collectionView1 layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0||indexPath.section==2)
    {
        return   CGSizeMake((collectionView1.frame.size.width),20);
    }
    else
    {
        return   CGSizeMake((collectionView1.frame.size.width/2)-7,83);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section==0 ||section==2)
    {
        return 1;
    }
    else
    {
        return  arrCategory.count;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell;
    
    
    
    
    // if (isList==FALSE)
    // {
    
    if (indexPath.section==0 || indexPath.section==2)
    {
        static NSString *identifier = @"blankcell";
        UICollectionViewCell *cell2 = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell = cell2;
    }
    else
    {
        NSDictionary *objCategory=[arrCategory objectAtIndex:indexPath.row];
        UICollectionViewCell   *CurrentSelectedGridCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"categoryList" forIndexPath:indexPath];
        UILabel *lblCategoryName = (UILabel *)[CurrentSelectedGridCell viewWithTag:101];
        [lblCategoryName setTextAlignment:NSTextAlignmentCenter];
        lblCategoryName.text=[NSString stringWithFormat:@"%@",[objCategory valueForKey:@"Title"]];
        
        CurrentSelectedGridCell.layer.borderWidth=1;
        CurrentSelectedGridCell.layer.borderColor=[UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1].CGColor;
        CurrentSelectedGridCell.layer.borderColor=[UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1].CGColor;
        cell= CurrentSelectedGridCell;
        
    }
    
    
    
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.section == 1)
    {
        NSDictionary *objCategory = [arrCategory objectAtIndex:indexPath.row];
//        CategoryDetailViewController *catDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryDetailViewController"];
//        catDetailViewController.catID = [objCategory valueForKey:@"Id"];
        
        WebViewViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewViewController"];
        vc.url = [NSURL URLWithString:[objCategory valueForKey:@"Url"]];
        vc.title = [objCategory valueForKey:@"Title"];
        vc.isFromDetail = @"yes";
//        catDetailViewController.objCategory = objCategory;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
