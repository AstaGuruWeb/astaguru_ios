//
//  CareersPageViewController.h
//  AstaGuru
//
//  Created by Amrit Singh on 12/28/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HowToBuyViewController.h"

@interface CareersPageViewController : UIViewController<UIPageViewControllerDelegate, HowToBuyDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedMenu;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@end
